package com.rabbani.flightchechin.integration;

import com.rabbani.flightchechin.integration.dto.Reservation;
import com.rabbani.flightchechin.integration.dto.ReservationUpdateRequest;

public interface ReservationRestClient {

	
	public Reservation findReservation(Long id);
	
	public Reservation updateReservation(ReservationUpdateRequest request);
}
